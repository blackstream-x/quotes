# Selected DevOps From Hell Excuses

> source: <https://botsin.space/@dofh_excuses>
>
> - Admin executed random code from Stack Overflow (1)
> - Solution hidden on second page of Google (8)
> - Cloud got struck by lightning (10)
> - Office 365 isn’t available on Feb 29th (11)
> - Cloud unavailable due to global warming (12)
> - Agile project team went missing over waterfall (14)
> - Construction work on route 53 (42)
> - WTFPL violation (70)
> - S3 Bucket has a hole (85)
> - Ruby derailed (91)
> - Serverless infrastructure provider ran out of servers (95)
> - Password too long (96)
> - Rolling release fell over (97)
> - We migrated from serverless to userless infrastructure (98)
> - We migrated from serverless to useless infrastructure (99)
> - CEO says we’re going fully Cloud Naive (104)
> - nginx out of fuel (106)
> 

